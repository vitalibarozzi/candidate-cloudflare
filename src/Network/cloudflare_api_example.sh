


declare -r CF_API_EMAIL="vitalibarozzi@pm.me"
declare -r CF_API_KEY="REDACTED"


# Will get account list
curl -X GET "https://api.cloudflare.com/client/v4/accounts?page=1&per_page=20&direction=desc" \
    -H "X-Auth-Email: ${CF_API_EMAIL}" \
    -H "X-Auth-Key: ${CF_API_KEY}" \
    -H "Content-Type: application/json"


# Will get account details 
curl -X GET "https://api.cloudflare.com/client/v4/accounts/${THIS_ACCOUNT_SHOULD_BE_EXTRACTED_FROM_JSON_RETURN_OF_OTHER_CALL}" \
    -H "X-Auth-Email: ${CF_API_EMAIL}" \
    -H "X-Auth-Key: ${CF_API_KEY}" \
    -H "Content-Type: application/json"


# Will create new zone
curl -X POST "https://api.cloudflare.com/client/v4/zones" \
    -H "X-Auth-Email: vitalibarozzi@pm.me" \
    -H "X-Auth-Key: eec8e4b33db07917b6e13cf325a03e121bb25" \
    -H "Content-Type: application/json" \
    --data '{"name":"1e3c.systems","account":{"id":"${ID_GOT_FROM_OTHER_CALL}","name":"${NAME_GOT_FROM_OTHER_CALL}","jump_start":true}'


