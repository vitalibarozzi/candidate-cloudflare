
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}


module Network.CloudFlare where


import Data.Text
import Data.Aeson as JSON
import Data.Text.Encoding
import Control.Lens as Lens
import System.Posix.Env
import GHC.Generics
import Network.HTTP.Simple
import           Control.Monad.IO.Class
import qualified Data.ByteString.Char8 as B8


data Credentials = Credentials
    { _creEmail :: Text
    , _creKey   :: Text
    } deriving
        ( Show
        )
Lens.makeLenses ''Credentials


-- | Will get the variables CF_API_EMAIL and CF_API_KEY from environment.
getCredentials :: IO Credentials
getCredentials = do
    env         <- getEnvironment
    filteredEnv <- pure
        [ (key,value) | (key,value) <- env
        , key == "CF_API_EMAIL" || key == "CF_API_KEY"
        ]
    pure $ Credentials "vitalibarozzi@pm.me" "eec8e4b33db07917b6e13cf325a03e121bb25"


data Settings = Settings
    { enforce_twofactor :: Bool
    } deriving
        ( Show
        , Generic
        )
instance FromJSON Settings


data Account = Account
    { id       :: Text
    , name     :: Text
    , settings :: Settings
    } deriving
        ( Show
        , Generic
        )
instance FromJSON Account


data ResultInfo = ResultInfo
    { page        :: Int
    , per_page    :: Int
    , count       :: Int
    , total_count :: Int
    } deriving
        ( Show
        , Generic
        )
instance FromJSON ResultInfo

-- "errors":[{"code":9106,"message":"Missing X-Auth-Email header"},{"code":9107,"message":"Missing X-Auth-Key header"}]
data Error = Error
    { _errCode :: Int
    , _errMessage :: Text
    } deriving
        ( Show
        , Generic
        )
Lens.makeLenses ''Error
instance FromJSON Error


data ListAccounts = ListAccounts
    { success    :: Bool
    , errors     :: [Error]
    , messages   :: [Text]
    , result     :: [Account]
    , result_info :: Maybe ResultInfo
    } deriving
        ( Show
        , Generic
        )
instance FromJSON ListAccounts


-- | https://api.cloudflare.com/#accounts-list-accounts
listAccounts :: Credentials -> IO ListAccounts
listAccounts cred = do
     
    rsp <- httpJSON req :: IO (Response ListAccounts)
    pure $ getResponseBody rsp

  where

    end = "https://api.cloudflare.com/client/v4/accounts?page=1&per_page=20&direction=desc"
    req = 
        addRequestHeader "Content-Type" "application/json"  $
        addRequestHeader "X-Auth-Email" (encodeUtf8 $ cred^.creEmail) $
        addRequestHeader "X-Auth-Key" (encodeUtf8 $ cred^.creKey) end

-- | https://api.cloudflare.com/#zone-create-zone
createZone :: Account -> IO 
createZone acc = do

    rsp <- httpJSON req :: IO (Response ListAccounts)
    pure $ getResponseBody rsp

  where

    end = "https://api.cloudflare.com/client/v4/zones"
    req = 
        addRequestHeader "Content-Type" "application/json"  $
        addRequestHeader "X-Auth-Email" (encodeUtf8 $ cred^.creEmail) $
        addRequestHeader "X-Auth-Key" (encodeUtf8 $ cred^.creKey) end
    
    

